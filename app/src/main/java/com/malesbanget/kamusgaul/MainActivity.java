package com.malesbanget.kamusgaul;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.malesbanget.kamusgaul.adapter.Word;
import com.malesbanget.kamusgaul.utils.ConnectionManager;
import com.malesbanget.kamusgaul.utils.DBHelper;
import com.malesbanget.kamusgaul.utils.GetDataTask;
import com.malesbanget.kamusgaul.utils.WordsDataSource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rheza on 12/20/2015.
 */
public class MainActivity extends Activity {
    private Button mBtnDownloadData;
    private Context mContext;
    private DataTaskListener mDataTaskListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        new DBHelper(mContext);
        setResource();
        setListener();
    }

    public interface DataTaskListener {
        void onDataTaskFinished(ArrayList<Word> words);
    }

    private void setResource() {
        mBtnDownloadData = (Button)findViewById(R.id.btn_download_db);
    }

    private void setListener() {
        mBtnDownloadData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchData();
            }
        });
    }

    private void fetchData() {
        mDataTaskListener = new DataTaskListener() {
            @Override
            public void onDataTaskFinished(ArrayList<Word> words) {
                ArrayList<String> savedWords = new ArrayList<String>();
                for(Word word:words) {
                    savedWords.add(word.getName());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                        android.R.layout.simple_dropdown_item_1line, savedWords);
                AutoCompleteTextView textView = (AutoCompleteTextView)
                        findViewById(R.id.editTextKata);
                textView.setAdapter(adapter);
            }
        };
        new GetDataTask(mContext, mDataTaskListener).execute();
    }
}
