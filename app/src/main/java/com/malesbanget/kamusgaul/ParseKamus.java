package com.malesbanget.kamusgaul;

/**
 * Created by Tonan Jihano on 30/09/2015.
 */
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;

public class ParseKamus extends Activity
{

    /** Called when the activity is first created. */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        setContentView(R.layout.activity_main);
        String input = readKamus();
        try {
            JSONObject json = new JSONObject(input);
            Log.i(ParseKamus.class.getName(), json.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String readKamus()
    {
        return "";
//        URL url = new URL("http://kamusslang.com/api/tonan/");
//        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//        conn.setRequestMethod("POST");
//
//// read the response
//        System.out.println("Response Code: " + conn.getResponseCode());
//        InputStream in = new BufferedInputStream(conn.getInputStream());
//        String response = org.apache.commons.io.IOUtils.toString(in, "UTF-8");
//        System.out.println(response);
//        return builder.toString();
    }
}
