package com.malesbanget.kamusgaul;

/**
 * Created by Tonan Jihano on 04/09/2015.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DataKamus extends SQLiteOpenHelper
{
    private static final String DEBUG_TAG = "KamusslangDatabase";
    private static final String DATABASE_NAME = "kamusgaul";
    public static final String TABLE_KAMUS_GAUL = "kamus_gaul";
    public static final String SLANG= "slang";
    public static final String INDONESIA = "indonesia";
    public static final String COL_wordtext ="";
    public static final String COL_meantext ="";

    //Constructor DataKamus untuk initiate database
    public DataKamus(Context context)
    {
        super(context, DATABASE_NAME, null, 1);
    }

    //method createTable untuk membuat table kamus
    public void createTable(SQLiteDatabase db)
    {
        db.execSQL("DROP TABLE IF EXISTS kamus");
        db.execSQL("CREATE TABLE if not exists kamus (id INTEGER PRIMARY KEY AUTOINCREMENT, slang TEXT, indonesia TEXT);");
    }

    //method generateData untuk mengisikan data ke kamus.
    public void generateData(SQLiteDatabase db)
    {
        ContentValues cv=new ContentValues();
        cv.put(SLANG, "asu");
        cv.put(INDONESIA, "Umpatan,makian yang meluapkan rasa jengkel,keki,donkol,dll");
        db.insert("kamus", SLANG, cv);
        cv.put(SLANG, "kimpo");
        cv.put(INDONESIA, "kimpoi = kawin");
        db.insert("kamus", SLANG, cv);
        cv.put(SLANG, "MBDC");
        cv.put(INDONESIA, "Singkatan dari malesbanget!dotcom, website kesukaan kita semua!");
        db.insert("kamus", SLANG, cv);
        cv.put(SLANG, "kimcil");
        cv.put(INDONESIA, "Daun muda perkotaan, biasanya berusia antara 15-23 tahun (SMA-Kuliah), bergaya trendi, suka begaol, cenderung sok imut, ceria-ceria bikin gemes, sasaran empuk cowok-cowok pinginan. \n" +
                "Epistemologi: \n" +
                "Singkatan dari kata 'kimpetan cilik\", artinya alat kelamin perempuan yang... masih 'kecil' (bisa dari usia atau ukuran :p).");
        db.insert("kamus", SLANG, cv);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        // TODO Auto-generated method stub
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        // TODO Auto-generated method stub
    }

}
