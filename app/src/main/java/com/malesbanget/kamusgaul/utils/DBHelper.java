package com.malesbanget.kamusgaul.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Rheza on 29/11/2015.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "kamus_gaul.sqlite";
    private static final int CURRENT_DATABASE_VERSION = 1;
    public static final String WORDS_TABLE = "WORDINGS";

    private static final String CREATE_TABLE_WORDS = "CREATE TABLE IF NOT EXISTS WORDINGS (id text primary key," +
            " name text," +
            " name_clean text," +
            " definition text," +
            " example text)";

    public DBHelper(Context context){
        super(context, DATABASE_NAME, null, CURRENT_DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("DBHelper", "Create Table");
        db.beginTransaction();
        try {
            db.execSQL(CREATE_TABLE_WORDS);
            db.setTransactionSuccessful();
            Log.i("DBHelper", "Create Table Done");
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (CURRENT_DATABASE_VERSION <= oldVersion) {
            return;
        }
    }

    public boolean insertFastToDB(String query, ArrayList<String> datas){
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransactionNonExclusive();
        SQLiteStatement statement = db.compileStatement(query);
        int i = 1;
        for(String data:datas) {
            statement.bindString(i, data);
            i++;
        }

        statement.execute();
        statement.clearBindings();
        db.setTransactionSuccessful();
        db.endTransaction();

        db.close();
        return true;
    }
}
