package com.malesbanget.kamusgaul.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.util.Log;

public class ConnectionManager {
    @SuppressWarnings("unused")
    private Context mContext;
    public static String BASE_URL = "http://kamusslang.com/api/tonan/";

    public final static int TIMEOUT_MILLISEC = 10000;  // = 10 seconds

    // RESPONSE CODE
    public final static int RESPONSE_SUCCESS = 200;

    public ConnectionManager(Context context) {
        mContext = context;
    }

    public HttpResponse fetchFileData(MultipartEntity entity) {
        if(entity != null) {
            HttpClient httpclient = new DefaultHttpClient();
            String url = BASE_URL;
            Log.i("ConnectionManager", "URL = "+url);
            HttpPost httppost = new HttpPost(url);
            HttpResponse response = null;
            try {
                httppost.setEntity(entity);
                response = httpclient.execute(httppost);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        } else {
            return null;
        }
    }

}
