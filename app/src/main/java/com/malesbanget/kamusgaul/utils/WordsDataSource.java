package com.malesbanget.kamusgaul.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.malesbanget.kamusgaul.adapter.Word;

import java.util.ArrayList;

/**
 * Created by Rheza on 30/11/2015.
 */
public class WordsDataSource {
    private SQLiteDatabase mDatabase;
    private DBHelper mDatabaseHelper;

    private Context mContext;

    public WordsDataSource(Context context) {
        mContext = context;
        mDatabaseHelper = new DBHelper(mContext);
    }

    public void open() throws SQLException {
        mDatabase = mDatabaseHelper.getWritableDatabase();
    }

    public void close() {
        mDatabaseHelper.close();
    }

    private Word cursorToWord(Cursor cursor) {
        Word word = null;
        try{
            word = new Word(cursor.getString(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4));
        } catch (Exception ex) {
            ex.getLocalizedMessage();
        }
        return word;
    }

    public boolean insertWords(Word word) {
        String query = "INSERT INTO WORDINGS ("+Word.COLUMN_NAMES[0]+", "+Word.COLUMN_NAMES[1]+", " +
                Word.COLUMN_NAMES[2]+", "+Word.COLUMN_NAMES[3]+", "+Word.COLUMN_NAMES[4]+") VALUES " +
                "(?, ?, ?, ?, ?)";
        ArrayList<String> words = new ArrayList<String>();
        words.add(word.getId());
        words.add(word.getName());
        words.add(word.getNameClean());
        words.add(word.getDefinition());
        words.add(word.getExample());

        return mDatabaseHelper.insertFastToDB(query, words);
//        boolean isSuccess = false;
//        String query = "INSERT INTO WORDINGS ("+Word.COLUMN_NAMES[0]+", "+Word.COLUMN_NAMES[1]+", " +
//                Word.COLUMN_NAMES[2]+", "+Word.COLUMN_NAMES[3]+", "+Word.COLUMN_NAMES[4]+
//                ") VALUES " +
//                "('"+word.getId()+"', '"+word.getName().replaceAll("'", "")+"', '"+word.getNameClean().replaceAll("'", "")+"', " +
//                "'"+word.getDefinition().replaceAll("'", "")+"', '"+word.getExample().replaceAll("'", "")+"')";
//        Log.i("WordsDataSource", "Query = "+query);
//        Cursor cursor = mDatabase.rawQuery(query, null);
//        if(cursor.getCount() > 0) isSuccess = true;
//        return isSuccess;
    }

    public Word getWordFromName(String key) {
        Cursor cursor = mDatabase.rawQuery(
                "SELECT * FROM "+DBHelper.WORDS_TABLE+" WHERE name = '"+key+"'"
                , null);
        cursor.moveToFirst();
        Word word = cursorToWord(cursor);
        return word;
    }

    public ArrayList<Word> getAllWords() {
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM "+DBHelper.WORDS_TABLE, null);
        ArrayList<Word> words = new ArrayList<Word>();
        if(cursor.moveToFirst()){
            while(cursor.isAfterLast() == false){
                Word word = cursorToWord(cursor);
                words.add(word);
                cursor.moveToNext();
            }
        }
        return words;

    }
}
