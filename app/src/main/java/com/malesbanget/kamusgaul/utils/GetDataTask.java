package com.malesbanget.kamusgaul.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.malesbanget.kamusgaul.MainActivity;
import com.malesbanget.kamusgaul.adapter.Word;

import org.apache.http.HttpResponse;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by Rheza on 20/12/2015.
 */
public class GetDataTask extends AsyncTask<Void, Void, Boolean> {
    private Context mContext;
    private MultipartEntity entity;
    private String mResultCode;
    private String mMessage;
    private String[] mStringResult;
    private boolean isFirstRequest = false;
    private ProgressDialog mProgressDialog;
//    private final static int MAX_PAGE = 156;
    private final static int MAX_PAGE = 1;
    private int mRequestedPage = 0;
    private final String PASSWORD = "favoritsetyakinikuteriyaki";
    private final String JSON_ID = "term_id";
    private final String JSON_NAME = "term_name";
    private final String JSON_NAME_CLEAN = "term_name_clean";
    private final String JSON_DEFINIITON = "term_definition";
    private final String JSON_EXAMPLE = "term_example";
    private WordsDataSource mWordDataSource;
    private MainActivity.DataTaskListener mDataTaskListener;

    public GetDataTask(Context context, MainActivity.DataTaskListener dataTaskListener){
        mContext = context;
        entity = new MultipartEntity();
        mProgressDialog = new ProgressDialog(mContext);
        mWordDataSource = new WordsDataSource(mContext);
        mDataTaskListener = dataTaskListener;
    }

    public void setIsFirstRequest(boolean firstRequest) {
        isFirstRequest = firstRequest;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mWordDataSource.open();
        mProgressDialog = mProgressDialog.show(mContext, "Loading...", "Mohon tunggu sebentar.", true);
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        ConnectionManager connManager = new ConnectionManager(mContext);
        proceedConnection(connManager, mRequestedPage);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        mProgressDialog.dismiss();
        Log.i("GetDataTask", "Done");
        sendBackData();
//        if(mStringResult != null) {
//            Toast.makeText(mContext, "Download finished", Toast.LENGTH_SHORT).show();
//        } else {
//            Toast.makeText(mContext, "Please check your network connection", Toast.LENGTH_SHORT).show();
//        }
    }

    private void sendBackData() {
        mWordDataSource.open();
        mDataTaskListener.onDataTaskFinished(mWordDataSource.getAllWords());
        mWordDataSource.close();
    }

    private void proceedConnection(ConnectionManager connectionManager, int pages) {
        try {
            entity.addPart("page", new StringBody(pages+""));
            entity.addPart("passwd", new StringBody(PASSWORD));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        HttpResponse response = connectionManager.fetchFileData(entity);
        if(response != null /*&& response.getEntity().getContentLength() > 0*/
                && response.getStatusLine().getStatusCode() != 500) {
            StringBuilder sb = new StringBuilder();
            try {
                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(response.getEntity().getContent()), 65728);
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                storeToDB(sb.toString());
                mRequestedPage++;
                if(mRequestedPage < MAX_PAGE) {
                    proceedConnection(connectionManager, mRequestedPage);
                }
                Log.i("ComplainActivity", "Server Response = " + sb);
            } catch (IOException e) {
                ((Activity)mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext, "Please check your network connection", Toast.LENGTH_SHORT).show();
                    }
                });
                e.printStackTrace();
            }
        }
    }

    private void storeToDB(String data) {
        try {
            JSONObject jsonData = new JSONObject(data);
            JSONArray dataArray = jsonData.getJSONArray("message");
            for(int i = 0; i < dataArray.length(); i++) {
                JSONObject temp = dataArray.getJSONObject(i);
                String id = temp.getString(JSON_ID);
                String name = temp.getString(JSON_NAME);
                String nameClean = temp.getString(JSON_NAME_CLEAN);
                String definition = temp.getString(JSON_DEFINIITON);
                String example = temp.getString(JSON_EXAMPLE);
                Word word = new Word(id, name, nameClean, definition, example);
                mWordDataSource.insertWords(word);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}