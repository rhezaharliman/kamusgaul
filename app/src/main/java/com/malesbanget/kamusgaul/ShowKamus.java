package com.malesbanget.kamusgaul;

/**
 * Created by Tonan Jihano on 04/09/2015.
 */
import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ShowKamus extends Activity
{
    private SQLiteDatabase db = null;
    private Cursor kamusCursor = null;
    private EditText txtSlang;
    private EditText txtIndonesia;
    private DataKamus datakamus = null;

    public static final String SLANG = "slang";
    public static final String INDONESIA = "indonesia";

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        datakamus = new DataKamus(this);
        db = datakamus.getWritableDatabase();
        datakamus.createTable(db);
        datakamus.generateData(db);

        setContentView(R.layout.activity_main);
        txtSlang = (EditText) findViewById(R.id.editTextKata);
        txtIndonesia = (EditText) findViewById(R.id.listHasilCariKata);
    }

    public void getKata(View view)
    {
        String result = "";
        String slangword = txtSlang.getText().toString();
        kamusCursor = db.rawQuery("SELECT ID, SLANG, INDONESIA "
                + "FROM kamus where SLANG='" + slangword
                + "' ORDER BY SLANG", null);

        if (kamusCursor.moveToFirst())
        {
            result = kamusCursor.getString(2);
            for (; !kamusCursor.isAfterLast(); kamusCursor.moveToNext())
            {
                result = kamusCursor.getString(2);
            }
        }

        if (result.equals(""))
        {
            result = "Terjemahan Not Found";
        }
        txtIndonesia.setText(result);
    }



    @Override
    public void onDestroy()
    {
        super.onDestroy();
        kamusCursor.close();
        db.close();
    }
}
