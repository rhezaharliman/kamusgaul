package com.malesbanget.kamusgaul;

/**
 * Created by Tonan Jihano on 04/09/2015.
 */
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.Provider;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

public class Downloader extends Service
{

    private static final String DEBUG_TAG = "";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        URL Path;
        try
        {
            Path = new URL(intent.getDataString());
            DownloaderTask download = new DownloaderTask();
            download.execute(Path);
        }
        catch (MalformedURLException e)
        {
            Log.e(DEBUG_TAG, "Bad URL", e);
        }
        return Service.START_FLAG_REDELIVERY;
    }

    @Override
    public IBinder onBind(Intent intent)
        {

        return null;

    }

    private class DownloaderTask extends AsyncTask<URL, Void, Boolean>
    {

        private static final String DEBUG_TAG = "";

        @Override
        protected Boolean doInBackground(URL... params)
        {
            boolean succeeded = false;
            URL downloadPath = params[0];

            if (downloadPath != null)
            {
                succeeded = xmlParse(downloadPath);
            }
            return succeeded;
        }

        private boolean xmlParse(URL downloadPath)
        {
            boolean succeeded = false;
            XmlPullParser pulling;
            try
            {
                pulling = XmlPullParserFactory.newInstance().newPullParser();
                pulling.setInput(downloadPath.openStream(), null);
                int eventType = -1;

                // for each found "item" tag, find "link" and "title" tags
                // before end tag "item"

                while (eventType != XmlPullParser.END_DOCUMENT)
                {
                    if (eventType == XmlPullParser.START_TAG)
                    {
                        String tagName = pulling.getName();
                        if (tagName.equals("item"))
                        {

                            ContentValues pullingData = new ContentValues();
                            // inner loop looking for link and title
                            while (eventType != XmlPullParser.END_DOCUMENT)
                            {
                                if (eventType == XmlPullParser.START_TAG)
                                {
                                    if (pulling.getName().equals("link"))
                                    {
                                        pulling.next();
                                        Log.d(DEBUG_TAG, "Link: " + pulling.getText());
                                        pullingData.put(DataKamus.COL_wordtext, pulling.getText());
                                    } else if (pulling.getName().equals("title"))
                                    {
                                        pulling.next();
                                        pullingData.put(DataKamus.COL_meantext, pulling.getText());
                                    }
                                }
                                else if (eventType == XmlPullParser.END_TAG)
                                {
                                    if (pulling.getName().equals("item"))
                                    {
                                        // save the data, and then continue with
                                        // the outer loop
//                                        getContentResolver().insert(Provider.CONTENT_URI, pullingData);
                                        break;
                                    }
                                }
                                eventType = pulling.next();
                            }
                        }
                    }
                    eventType = pulling.next();
                }
                // no exceptions during parsing
                succeeded = true;
            }
            catch (XmlPullParserException e)
            {
                Log.e(DEBUG_TAG, "Error during parsing", e);
            }
            catch (IOException e)
            {
                Log.e(DEBUG_TAG, "IO Error during parsing", e);
            }
            return succeeded;
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            if (!result)
            {
                Log.w(DEBUG_TAG, "XML download and parse had errors");
            }
        }
    }
}