package com.malesbanget.kamusgaul;

/**
 * Created by Tonan Jihano on 04/09/2015.
 */
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

/**
 * Created by Tonan Jihano on 02/09/2015.
 */
public class AlarmReceiver extends BroadcastReceiver {

    private static final String DEBUG_TAG = "AlarmReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(DEBUG_TAG, "Recurring alarm; requesting download service.");
        // start the download
        Intent downloader = new Intent(context, Downloader.class);
        // url
        downloader.setData(Uri.parse(""));
        context.startService(downloader);
    }

}
