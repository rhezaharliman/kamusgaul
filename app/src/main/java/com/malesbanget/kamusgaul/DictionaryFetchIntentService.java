package com.malesbanget.kamusgaul;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by Tonan Jihano on 07/08/2015.
 */
public class DictionaryFetchIntentService  extends IntentService
{
    public DictionaryFetchIntentService()
    {
        super("DictionaryFetchIntentService");
    }

    @Override
    protected void onHandleIntent(Intent workingIntent)
    {
        String url = workingIntent.getDataString();
        int result = Activity.RESULT_CANCELED;

        // A URL that's local to this method

        InputStream stream = null;
        try
        {
            URL streamURL = new URL(url);
            stream = streamURL.openConnection().getInputStream();
            InputStreamReader reader = new InputStreamReader(stream);

            // From here,
            // If it's JSON, then change the InputStream into String
            // if it's file, then change the InputStream into FileOutputStream

            // successfully finished
            result = Activity.RESULT_OK;

            // if there's no problem, store the
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (stream != null)
            {
                try
                {
                    stream.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
//            if (fos != null) {
//                try {
//                    fos.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
        }
    }
    private void setRecurringAlarm(Context context)
    {
        Calendar updateTime = Calendar.getInstance();
        updateTime.setTimeZone(TimeZone.getTimeZone("GMT"));
        updateTime.set(Calendar.MINUTE, 02);

        Intent downloader = new Intent(context, AlarmReceiver.class);
        PendingIntent recurringDownload = PendingIntent.getBroadcast(context, 0, downloader, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarms = (AlarmManager) getApplication().getSystemService(Context.ALARM_SERVICE);
        alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP, updateTime.getTimeInMillis(), AlarmManager.INTERVAL_DAY, recurringDownload);
    }
}
