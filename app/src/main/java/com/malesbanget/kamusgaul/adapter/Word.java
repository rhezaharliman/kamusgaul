package com.malesbanget.kamusgaul.adapter;

/**
 * Created by Rheza on 30/11/2015.
 */
public class Word {
    private String mId;
    private String mName;
    private String mNameClean;
    private String mDefinition;
    private String mExample;

    public static final class COLUMN_IDS {
        public static final int WORD_ID = 0;
        public static final int WORD_NAME = 1;
        public static final int WORD_NAME_CLEAN = 2;
        public static final int WORD_DEFINITION = 3;
        public static final int WORD_EXAMPLE = 4;
    }

    public static final String[] COLUMN_NAMES = {
            "id", "name", "name_clean", "definition", "example"
    };

    public Word(String id, String name, String nameClean, String definition, String example) {
        mId = id;
        mName = name;
        mNameClean = nameClean;
        mDefinition = definition;
        mExample = example;
    }

    public String getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getNameClean() {
        return mNameClean;
    }

    public String getDefinition() {
        return mDefinition;
    }

    public String getExample() {
        return mExample;
    }
}
